import {Fragment, useEffect, useState, useContext} from 'react'

import UserContext from "./../UserContext"

// import orderData from './../data/orderData';

import OrderCard from './../components/OrderCard';
import AdminView from "./../components/AdminView";
import UserView from "./../components/UserView";

export default function Order(){

	const [orders, setOrder] = useState([])
	const {user} = useContext(UserContext)

	const fetchData = () => {
		fetch("http://localhost:4000/api/orders/products")
		.then(response => response.json())
		.then(data => {
			setOrder(data)
		})
	}
	
	useEffect(() => {
		fetchData()
	}, [])

	return (
		<Fragment>
			{
				(user.isAdmin === true ) ?
					<AdminView orderData= {orders} fetchData={fetchData}/>
				:
					<UserView orderData={orders}/>
			}
		</Fragment>
	)
}