import {useEffect, useState, useContext} from 'react'
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, useHistory, Link} from 'react-router-dom'

import UserContext from './../UserContext'

import Swal from "sweetalert2";

export default function OrderView(){
	const [orderItem, setOrderItem] = useState("")
	const [description, setDescription] = useState("")
	const [orderQty, setOrderQty] = useState("")
	const [price, setPrice] = useState(0)

	const {user} = useContext(UserContext)


	const {orderId} = useParams()

	let history = useHistory();

	useEffect( () => {

		fetch(`http://localhost:4000/api/orders/products/${orderId}`, {
			headers: {
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(response => response.json())
		.then(data => {
			console.log(data) //object

			setOrderItem(data.orderItem)
			setDescription(data.description)
			setPrice(data.price)

		})
	}, [orderId])

	const buy = (orderId) => {

		fetch('http://localhost:4000/api/orders/create-order', {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				orderId: orderId
			})
		})
		.then(response => response.json())
		.then(data => {
			

			if(data === true){
				Swal.fire({
					title: "Order",
					icon: "success",
					text: "You have Successfully Placed an Order"
				})

				history.push("/order")
			} else {
				Swal.fire({
					title: "Something went wrong.",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	return(
		<Container>
			<Row className="justify-content-center">
				<Col xs={8} md={4}>
					<Card>
						<Card.Body>
							<Card.Title>Name:</Card.Title>
							<Card.Text>{orderItem}</Card.Text>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Qty:</Card.Subtitle>
							<Card.Text>{orderQty}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>{price}</Card.Text>

							{
								(user.id !== null) ?
									<Button onClick={() => buy(orderId)}>Buy</Button>
								:
									<Link 
										className="btn btn-primary" 
										to="/login">Login to Edit Orders</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}