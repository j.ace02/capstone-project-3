import {useState, useEffect, useContext} from 'react';
import {Container, Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2'; 
import {Redirect} from 'react-router-dom';

import UserContext from '../UserContext';

export default function Register(){

	const { user, setUser } = useContext(UserContext);

	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [cpw, setCpw] = useState("")
	const [mobileNo, setMobileNo] = useState("")
	
    
	const [isDisabled, setIsDisabled] = useState(true)


	useEffect( ()=>{
		if (
			(
				firstName !== "" &&
				lastName !== "" &&
				email !== "" && 
				password !== "" && 
				cpw !== "" &&
				mobileNo !==""
			) 
				&& 
		    (password === cpw))
		{
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}

	}, [firstName,lastName,email, password, cpw, mobileNo])


	function Register(e){
		e.preventDefault()

		//check if the values are successfully bounded.
		console.log(email);
		console.log(password);
		console.log(cpw);
        
        
		fetch(`http://localhost:4000/api/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password,
				mobileNo: mobileNo
			})
		}).then(response => response.json())
		  .then(convertedData => {
			console.log(convertedData); 

			//create a control structure
			if (convertedData === true) {
				setFirstName("")
				setLastName("")
				setEmail("")
				setPassword("")
				setCpw("")
				setMobileNo("")
              //successful 
              Swal.fire({
              	title: 'Account Created Successfully',
              	icon: 'success', 
              	text: 'Congrats'
              })
			} else { 
              //unsuccessful 
              Swal.fire({
              	title: 'Account Creation Failed',
              	icon: 'error', 
              	text: 'Something Went Wrong'
              })
			}
		})
	}

	return(

		<Container>
			<Form 
				className="border p-3 mb-3"
				onSubmit={ (e) => Register(e) }
			>

			{/*FirstName*/}
			<Form.Group className="mb-3" controlId="email">
			  <Form.Label>First Name</Form.Label>
			  <Form.Control 
			  	type="text" 
			  	placeholder="Enter Your First Name" 
			  	value={firstName} 
			  	onChange={ (e) => setFirstName(e.target.value) }
			  />
			</Form.Group>

			{/*LastName*/}
			<Form.Group className="mb-3" controlId="email">
			  <Form.Label>Last Name</Form.Label>
			  <Form.Control 
			  	type="text" 
			  	placeholder="Enter Your Last Name" 
			  	value={lastName} 
			  	onChange={ (e) => setLastName(e.target.value) }
			/>
			</Form.Group>

			{/*email*/}
			  <Form.Group className="mb-3" controlId="email">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    	type="email" 
			    	placeholder="Enter email" 
			    	value={email} 
			    	onChange={ (e) => setEmail(e.target.value) }
			    	/>
			  </Form.Group>
			  

			{/*password*/}
			  <Form.Group className="mb-3" controlId="password">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Password" 
			    	value={password} 
			    	onChange={ (e) => setPassword(e.target.value) }
			    	/>
			  </Form.Group>


			{/*confirm password*/}
			<Form.Group className="mb-3" controlId="cpw">
			  <Form.Label>Confirm Password</Form.Label>
			  <Form.Control 
			  		type="password" 
			  		placeholder="Verify Password" 
			  		value={cpw} 
			  		onChange={ (e) => setCpw(e.target.value) }
			  		/>
			</Form.Group>

			{/*Mobile Number*/}
			<Form.Group className="mb-3" controlId="cpw">
			  <Form.Label>Mobile Number</Form.Label>
			  <Form.Control 
			  		type="text" 
			  		placeholder="Enter Your Mobile Number" 
			  		value={mobileNo} 
			  		onChange={ (e) => setMobileNo(e.target.value) }
			  />
			</Form.Group>

			  <Button 
			  		variant="primary" 
			  		type="submit"
			  		disabled={isDisabled}
			  		>
			    Submit
			  </Button>

			</Form>
		</Container>
	)
}