
const orderData = [
	{
		id:"ord0001",
		name: "Banana Cake",
		description:"A scrumptious cake made with real banana.",
		price: 130,
		onOffer: true
	},
	{
		id:"ord0002",
		name: "Chocolate Brownies",
		description:"A chocolate fudgey cake that has a glossy skin on the upper crust.",
		price: 285,
		onOffer: true
	},
	{
		id:"ord0003",
		name: "Carrot Cake",
		description:"A cake that contains with real carrots into a batter. A secret receipe has been added to ensure the cake's unique food taste and quality.",
		price: 240,
		onOffer: true
	}
]

export default orderData