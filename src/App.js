import {useState, useEffect} from 'react'
import {Container} from 'react-bootstrap'
import {BrowserRouter, Switch, Route} from 'react-router-dom';

import './bootstrap.min.css'; 

//for React Context
import { UserProvider } from './UserContext';

/*components*/
import AppNavbar from './components/AppNavbar';

/*pages*/
import Home from './pages/Home';
import Order from './pages/Order';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import ErrorPage from './pages/Error';
import OrderView from './pages/OrderView';


export default function App() {
    let token = localStorage.getItem("token")
    console.log(token)
   
    const [user, setUser] = useState({
              id: null,
              isAdmin: null
          })

    //function for clearing localStorage on logout
    const unsetUser = () => {
          localStorage.clear();

          setUser({
            id: null,
            isAdmin: null
          })
    }


    useEffect( () => {

      fetch("http://localhost:4000/api/users/details", {
            headers: {
              "Authorization": `Bearer ${token}`
            }
      })
      .then(response => response.json())
      .then(data => {
        console.log(data) 
          if(typeof data._id !== "undefined"){
            setUser({
              id: data._id,
              isAdmin: data.isAdmin
            })
          }
      })
    }, [token])

  return(
    <UserProvider value={{ user, setUser, unsetUser}}>
      <BrowserRouter>
        <AppNavbar/>
          <Container fluid className="m-3">
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/order" component={Order} />
              <Route exact path="/order/:orderId" component={OrderView} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/logout" component={Logout} />
              <Route component={ErrorPage} />
            </Switch>
          </Container>
      </BrowserRouter>
    </UserProvider>
    )
} 