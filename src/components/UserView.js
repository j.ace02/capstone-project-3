import {useState, useEffect, Fragment} from 'react';
import OrderCard from './OrderCard';

export default function UserView({orderData}) {
	const [orders, setOrders] = useState([])

	useEffect( () => {
		const orderArr = orderData.map( order => {
			if(order.isActive === true){
				return(
					//pass each element of the array to CourseCard component
					<OrderCard 
						orderProp={order} 
						key={order._id}
					/>
				)
			} else {
				return null
			}
		})

		setOrders(orderArr)

	}, [orderData])


	return(
		<Fragment>
			{orders}
		</Fragment>
	)
}