import {useState, Fragment, useEffect} from 'react'
import {Container, Button, Row, Col, Table, Modal, Form} from 'react-bootstrap';

import Swal from 'sweetalert2';

export default function AdminView(props) {

	const {orderData, fetchData} = props

	const [stockItem, setStockItem] = useState("")
	const [stockDescription, setStockDescription] = useState("")
	const [stockQty, setStockQty] =  useState("")
	const [stockPrice, setStockPrice] = useState(0)
	const [sellingPrice, setSellingPrice] = useState(0)

	const [orderId, setOrderId] = useState("")
	const [orders, setOrders] = useState([])

	// add orders button state
	const [showAdd, setShowAdd] = useState(false)

	//function to open & close button
	const openAdd = () => setShowAdd(true)
	const closeAdd = () => setShowAdd(false)

	useEffect( () => {
		const ordersArr = orderData.map(ordered => {

			return(
				<tr>
					<td>{ordered.stockItem}</td>
					<td>{ordered.stockDescription}</td>
					<td>{ordered.stockQty}</td>
					<td>{ordered.stockPrice}</td>
					<td>{ordered.sellingPrice}</td>
					<td>
						{
							(ordered.isActive === true) ?
								<span>Available</span>
							:
								<span>Unavailable</span>
						}
					</td>
					<td>
						<Button onClick={() => openEdit(ordered._id)}>Update</Button>
						{
							(ordered.isActive) ?
								<Fragment>
									<Button 
										variant="danger"
										onClick={ () => archiveOrder(ordered._id, ordered.isActive)}
										>Disable</Button>
									<Button variant="secondary">Delete</Button>
								</Fragment>
							:
								<Fragment>
									<Button variant="success">Enable</Button>
									<Button variant="secondary">Disable</Button>
								</Fragment>
								
						}
					</td>
				</tr>
			)
		})

		setOrderId(ordersArr)

	}, [orderData])

	/*Functions*/

		const addOrder = (e) => {
			e.preventDefault()

			fetch("http://localhost:4000/api/stocks/addStock", {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem("token")}`
				},
				body: JSON.stringify({
					stockItem: stockItem,
					stockDescription: stockDescription,
					stockQty: stockQty,
					stockPrice: stockPrice,
					sellingPrice: sellingPrice
				})
			})
			.then(response => response.json())
			.then(data => {
				// console.log(data)

				if(data == true){

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "orders successfully added"
					})

					//setting back to original state after successfully added the orders
					setStockItem("")
					setStockDescription("")
					setStockQty(0)
					setStockPrice(0)
					setSellingPrice(0)

					//close the modal after the alert
					closeAdd();

					//function passed from orderss page component to retrive all orderss
					fetchData()

				} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again"
					})

					fetchData()
				}
			})
		}

		//function to populate data in the form upon clicking Update button
		const openEdit = (orderId) => {

			fetch(`http://localhost:4000/api/orders/${orderId}`, {
				headers: {
					"Authorization": `Bearer ${localStorage.getItem("token")}`
				}
			})
			.then(response => response.json())
			.then(data => {

				setStockItem(data.stockItem)
				setStockDescription(data.stockDescription)
				setStockQty(data.stockQty)
				setStockPrice(data.stockPrice)
				setSellingPrice(data.sellingPrice)

			})

			setShowAdd(true)

		}

		
		const editOrder = (e, orderId) => {
			e.preventDefault()

			fetch(`http://localhost:4000/api/stocks/${orderId}/replenish`, {
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem("token")}`
				},
				body: JSON.stringify({
					stockItem: stockItem,
					stockDescription: stockDescription,
					stockQty: stockQty,
					stockPrice: stockPrice,
					sellingPrice: sellingPrice
				})
			})
			.then(response => response.json())
			.then(data => {
		
				if(typeof data !== "undefined"){
					fetchData()

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "orders successfully updated."
					})

					closeAdd()
				} else {

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					})

					fetchData()
				}
			})
		}

		//archive orders
		const archiveOrder = (isActive) => {
			fetch(`http://localhost:4000/api/stocks/archive-stocks`, {
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem("token")}`
				},
				body: JSON.stringify({
					isActive: !isActive
				})
			})
			.then(response => response.json())
			.then(data => {
				// console.log(data)

				if(data === true){

					fetchData()

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "orders disabled"
					})
				} else {
					fetchData()

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					})
				}
			})
		}

	return(
		<Container>
			<h2 className="text-center">Admin Dashboard</h2>
			<Row className="justify-content-center">
				<Col>
					<div className="text-right">
						<Button onClick={openAdd}>Add New orders</Button>
					</div>
				</Col>
			</Row>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>Stock Item</th>
						<th>Stock Description</th>
						<th>Stock Price</th>
						<th>Selling Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{orders}
				</tbody>
			</Table>

		{/*Add orders Modal*/}
		<Modal show={showAdd} onHide={closeAdd}>
			<Modal.Header closeButton>
				<Modal.Title>Add orders</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<Form onSubmit={ (e) => addOrder(e)}>
					<Form.Group controlId="stockItem">
						<Form.Label>Stock Item:</Form.Label>
						<Form.Control 
							type="text"
							value={stockItem}
							onChange={ (e) => setStockItem(e.target.value)}/>
					</Form.Group>
					<Form.Group controlId="stockDescription">
						<Form.Label>Stock Description:</Form.Label>
						<Form.Control 
							type="text" 
							value={stockDescription}
							onChange={ (e) => setStockDescription(e.target.value)}/>
					</Form.Group>
					<Form.Group controlId="stockPrice">
						<Form.Label>Stock Price:</Form.Label>
						<Form.Control 
							type="number" 
							value={stockPrice}
							onChange={ (e) => setStockPrice(e.target.value)}/>
					</Form.Group>
					<Form.Group controlId="sellingPrice">
						<Form.Label>Selling Price:</Form.Label>
						<Form.Control 
							type="number" 
							value={sellingPrice}
							onChange={ (e) => setSellingPrice(e.target.value)}/>
					</Form.Group>
					<Button variant="success" type="submit">
					    Submit
					</Button>
					<Button 
						variant="secondary" 
						type="submit" 
						onClick={closeAdd}>
					    Close
					</Button>
				</Form>
			</Modal.Body>
		</Modal>

		{/*Edit orders*/}
		<Modal show={showAdd} onHide={closeAdd}>
			<Modal.Header closeButton>
				<Modal.Title>Edit Orders</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<Form onSubmit={ (e) => editOrder(e, orderId)}>
					<Form.Group controlId="stockItem">
						<Form.Label>Stock Item:</Form.Label>
						<Form.Control 
							type="text"
							value={stockItem}
							onChange={ (e) => setStockItem(e.target.value)}/>
					</Form.Group>
					<Form.Group controlId="stockDescription">
						<Form.Label>Stock Description:</Form.Label>
						<Form.Control 
							type="text" 
							value={stockDescription}
							onChange={ (e) => setStockDescription(e.target.value)}/>
					</Form.Group>
					<Form.Group controlId="stockPrice">
						<Form.Label>Stock Price:</Form.Label>
						<Form.Control 
							type="number" 
							value={stockPrice}
							onChange={ (e) => setStockPrice(e.target.value)}/>
					</Form.Group>
					<Form.Group controlId="sellingPrice">
						<Form.Label>Selling Price:</Form.Label>
						<Form.Control 
							type="number" 
							value={sellingPrice}
							onChange={ (e) => setSellingPrice(e.target.value)}/>
					</Form.Group>
					<Button variant="success" type="submit">
					    Submit
					</Button>
					<Button 
						variant="secondary" 
						type="submit" 
						onClick={closeAdd}>
					    Close
					</Button>
				</Form>
			</Modal.Body>
		</Modal>

		</Container>	
	)
}