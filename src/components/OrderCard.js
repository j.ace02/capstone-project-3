
import {Container, Row, Col, Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function OrderCard({orderProp}){

	const {orderName, description, price, _id} = orderProp

	return(
		<Container>
			<Row className="justify-content-center">
				<Col xs={8} md={4}>
					<Card>
					  <Card.Body>
					    <Card.Title>{orderName}</Card.Title>
					    <Card.Subtitle>Description:</Card.Subtitle>
					    <Card.Text>{description}</Card.Text>
					    <Card.Subtitle>Price:</Card.Subtitle>
					    <Card.Text>{price}</Card.Text>
					    <Link className="btn btn-primary" to={`/order/${_id}`}>Details</Link>
					  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}