
import {Container, Carousel, img, Row, Col} from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';

/*Pictures*/
import Carousel1 from './../images/carousel/Carousel1.png';
import food_quiz from './../images/carousel/food-quiz.png';
import orderNow from './../images/carousel/orderNow.png';

export default function Banner(){

	return(
		<Container>
			<Row>
				<Col>
					<div className="carousel">
					<Carousel>
					  <Carousel.Item>
					    <img
					      className="d-block w-100"
					      src={Carousel1}
					      alt="First slide"
					      height={600}
					      width={50}
					    />
					    
					  </Carousel.Item>


					  <Carousel.Item>
					  	<a href="https://forms.gle/A9CcRDfZwRG2jyg7A" target="_blank">
						    <img
						      className="d-block w-100"
						      src={food_quiz}
						      alt="Second slide"
						      height={600}
						      width={50}
						    />

					   </a>

					  </Carousel.Item>

					  <Carousel.Item>
					    <img
					      className="d-block w-100"
					      src={orderNow}
					      alt="Third slide"
					      height={600}
					      width={50}
					      onClick={
					      	<Redirect to="/order"/>
					      }
					    />
					  </Carousel.Item>
					</Carousel>
					</div>
				</Col>
			</Row>
		</Container>

	)
}