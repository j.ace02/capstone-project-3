import { Card, Button } from 'react-bootstrap'


/*Pictures*/
import banana_cake from './../images/Foods/banana_cake.png';
import review from './../images/review.png';
import support from './../images/support.png';

export default function Highlights(){

	return(


		<div className="container mt-4">
			<div className="row mb-3">
				{/*card-1*/}
				<div className="col-10 col-md-4">
					<Card style={{ width: '20rem'}}>
					  <Card.Img variant="top" src={banana_cake} />
					  <Card.Body>
					    <Card.Title>Our Best Seller!</Card.Title>
					    <Button className="btn-primary">Learn More</Button>
					  </Card.Body>
					</Card>
				</div>


			{/*card-2*/}
				<div className="col-10 col-md-4">
					<Card style={{ width: '20rem'}}>
					  <Card.Img variant="top" src={review} />
					  <Card.Body>
					    <Card.Title>Customer Review</Card.Title>
					    <Button className="btn-primary">Learn More</Button>
					  </Card.Body>
					</Card>
				</div>

			{/*card-3*/}
				<div className="col-10 col-md-4 ml-5">
					<Card style={{ width: '20rem'}}>
					<a href="ContactForm.html">
						<Card.Img variant="top" src={support} />
					</a>
					</Card>
				</div>


			</div>	
		</div>
	)
}